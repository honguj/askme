class AddSubmittedToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :submitted, :boolean
  end
end
