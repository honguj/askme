class AddSelectedAnswerToQuestions < ActiveRecord::Migration
  def change
    change_table :questions do |t|
        t.references :selectedAnswer
    end
  end
end
