class AddOwnerToQuestions < ActiveRecord::Migration
  def change
    change_table :questions do |t|
        t.references :owner
    end
  end
end
