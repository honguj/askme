class AddAnswererToQuestions < ActiveRecord::Migration
  def change
    change_table :questions do |t|
        t.references :answerer
    end
  end
end
