class CreateChallenges < ActiveRecord::Migration
  def change
    create_table :challenges do |t|
      t.references :question
      t.references :answerer
      t.references :asker
      t.references :selectedAnswer

      t.timestamps
    end
  end
end
