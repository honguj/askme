class Question < ActiveRecord::Base
    validates :title, presence: true
    validates :content, presence:true
    validates :answerer_id, presence: true
    #validate :has_answers? # Has to disable this to provide copy question => bug to fix
    
    has_many :answers, dependent: :destroy
    accepts_nested_attributes_for :answers
    has_one :selectedAnswer, :class_name => 'Answer'
    
    belongs_to :answerer, :class_name => 'User', dependent: :destroy
    belongs_to :owner, :class_name => 'User', dependent: :destroy
    
    def has_answers?
        unless self.answers.size > 1
          self.errors[:question] << 'must have at least 2 answers'
        end
    end
    
    def answers_for_form
        collection = answers.where(question_id: id)
        collection.any? ? collection : answers.build
    end
    
    def self.search(query)
        # where(:title, query) -> This would return an exact match of the query
        where("title like ?", "%#{query}%") 
    end
    
end
