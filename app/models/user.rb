class User < ActiveRecord::Base
    has_many :friendships
    has_many :friends, :through => :friendships, :conditions => "status = 'accepted'"
    has_many :pending_friends, :through => :friendships, :conditions => "status = 'pending'" , :source => :friend
    #has_many :requested_friends, :through => :friendships, :conditions => "status = 'requested'"
    
    has_many :inverse_friendships, :class_name => "Friendship", :foreign_key => "friend_id"
    has_many :inverse_friends, :through => :inverse_friendships, :source => :user, :conditions => "status = 'accepted'"
    has_many :requested_friends, :through => :inverse_friendships, :conditions => "status = 'pending'", :source => :user
    #has_many :inverse_pending_friends, :through => :inverse_friendships, :conditions => "status = 'requested'"
    
    has_many :questions
    def self.from_omniauth(auth)
        where(auth.slice(:provider, :uid)).first_or_initialize.tap do |user|
            user.provider = auth.provider
            user.uid = auth.uid
            user.name = auth.info.name
            user.oauth_token = auth.credentials.token
            user.oauth_expires_at = Time.at(auth.credentials.expires_at)
            user.save!
        end
    end
end
