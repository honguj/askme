class WelcomeController < ApplicationController
    layout "application"
    
  def index
      @user = current_user
      @questions = Question.where('submitted = true').order("updated_at DESC")
  end
end
