class ChallengesController < ApplicationController
    
    def index
        @questions = Question.where(answerer_id: current_user.id).where(submitted: true).order("updated_at DESC")
        @status = Hash.new
        @questions.each do |q|
            if q.selectedAnswer_id
                @answer = Answer.find(q.selectedAnswer_id)
                if @answer.correct
                    @status[q.id] = 'Success'
                else
                    @status[q.id] = 'Fail'
                end
            else
                @status[q.id] = 'Answer'
            end
            @status[q.id]
        end
    end
    
    def create
        @user = current_user
        @question = Question.find(params[:id])
        
        # Verify if the current user is friend with the question owner
        if @user.friends.include?(@question.owner) or @user.inverse_friends.include?(@question.owner)
            #Create a new question by copying from old question
            @newQuestion = Question.create(title: @question.title, content: @question.content, owner_id: @question.owner_id, submitted: true, answerer_id: @user.id)
            for a in @question.answers do
                @answer = @newQuestion.answers.create( answer: a.answer, correct: a.correct )
            end    

            if @newQuestion.save
                redirect_to edit_challenge_path(@newQuestion)
            end
        else
            render 'questions/show'
        end
    end
    
    def edit
        @question = Question.find(params[:id])
        if @question.selectedAnswer_id
            redirect_to questions_path
        end
    end
    
    def update
        @question = Question.find(params[:id])
        @answer = Answer.find(params[:correct])
        @question.selectedAnswer_id = params[:correct]
        if @question.update(question_params)
            redirect_to @question
        else
            render 'edit'
        end 
    end
    
     private
        def question_params
           params.require(:question).permit(:title, :content, :answerer_id,
               answers_attributes: [:id, :answer, :question_id, :correct, :_destroy]) 
        end
end
