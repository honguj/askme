class FriendshipsController < ApplicationController
    def show
        @user= current_user 
        @users = User.all
    end
    
    def create
        @friendship = current_user.friendships.build(:friend_id => params[:friend_id], :status => 'pending')
        if @friendship.save
            flash[:notice] = "Added friend."
            redirect_to friendship_path(current_user)
        else
            flash[:error] = "Unable to add friend."
            redirect_to friendship_path(current_user)
        end
    end
    
    def update
        @user = current_user
        @friend = User.find(params[:id])
        @friendship = Friendship.find_by_user_id_and_friend_id(@friend, @user)
        @friendship.update_attributes(:status => 'accepted')
        redirect_to friendship_path(current_user)
    end
    
    def destroy
        @friendship = current_user.friendships.find(params[:id])
        @friendship.destroy
        flash[:notice] = "Removed friendship."
        redirect_to friendship_path(current_user)
    end
end
