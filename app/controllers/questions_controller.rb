class QuestionsController < ApplicationController
    layout "application"
    autocomplete :question, :title
    
    def new
        @question = Question.new
        @user = current_user
        @friends = []
        for friendship in @user.friendships do
            @friends << User.find(friendship.friend)
        end
    end
    
    def create
        @question = Question.new(question_params)
        @question.owner_id = session[:user_id]
        @question.submitted = false
        answerer = User.where("name = ?", "#{params[:user_name]}").limit(1)
        @question.answerer = answerer[0]
        if @question.save
            redirect_to edit_question_path(@question)
        else
            render 'new'
        end
    end
    
    def show
        @question = Question.find(params[:id]) 
        @user = current_user
    end
    
    def index
        if params.has_key?(:question)
          @questions = Question.search(params[:question][:title]).order("created_at DESC")
        else
          @questions = Question.where(owner_id: current_user.id).order("created_at DESC")
        end
    end
    
    def edit
        @question = Question.find(params[:id])
        if @question.submitted
            redirect_to questions_path
        end
    end
    
    def update
        @question = Question.find(params[:id])
        @answer = Answer.find(params[:correct])
        @answer.correct = true
        @question.submitted = true;
        if @question.update(question_params) and @answer.save
            redirect_to @question
        else
            render 'edit'
        end 
    end
    
    def destroy
      @question = Question.find(params[:id])
      @question.destroy

      redirect_to questions_path
    end
    
    private
        def question_params
           params.require(:question).permit(:title, :content, :answerer_id,
               answers_attributes: [:id, :answer, :question_id, :correct, :_destroy]) 
        end
end
