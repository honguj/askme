class ApplicationController < ActionController::Base
    before_action :require_login
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
    protect_from_forgery with: :exception
    helper_method :current_user
    
    def current_user
        if session[:user_id].nil?
            return nil
        else
            @current_user ||= User.find(session[:user_id]) 
        end
    end
    
    def require_login
      @current_user = current_user
      if @current_user.nil? 
        flash[:error] = "You must be logged in to access this section"
        redirect_to root_path
      end
    end
end
