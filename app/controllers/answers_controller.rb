class AnswersController < ApplicationController
    
    def create
        @question = Question.find(params[:question_id])
        @answer = @question.answers.create( answer: params[:answer][:answer], correct: false)
        redirect_to question_path(@question)
      end
    
    def destroy
      @question = Question.find(params[:question_id])
      @answer = Answer.find(params[:id])
      @answer.destroy

      redirect_to question_path(@question)
    end
end
