class UsersController < ApplicationController
    def index
        
    end
    
    def autocomplete_user_name
        term = params[:term]
        @current_user = current_user
        @friends = []
        for friendship in @current_user.friendships do
            @f = User.find(friendship.friend)
            @u = User.find(friendship.user)
            @friends << @u.id
            @friends << @f.id
        end
            
        for friendship in @current_user.inverse_friendships do
            @u = User.find(friendship.user)
            @f = User.find(friendship.friend)
            @friends << @u.id
            @friends << @f.id
        end
        
        @users = User.where('name LIKE ? AND id IN (?)', "%#{term}%", @friends)
           
        render :json => @users.map { |user| {:id => user.id, :label => user.name, :value => user.name} }
    end
end
