    OmniAuth.config.logger = Rails.logger
     
    Rails.application.config.middleware.use OmniAuth::Builder do
        provider :google_oauth2, GoogleapiYetting.google_oauth2_id, GoogleapiYetting.google_oauth2_secret , {client_options: {ssl: {ca_file: Rails.root.join("cacert.pem").to_s}}}
    end